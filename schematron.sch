<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2"
    xmlns:sqf="http://www.schematron-quickfix.com/validator/process"
    xmlns:gd="urn:vse:feja02:gameDeals">
    
    <sch:ns prefix="gd" uri="urn:vse:feja02:gameDeals"/>
    <sch:title>Kontrola GameDeals databáze</sch:title>
    
    <sch:pattern>
        <sch:title>Deal nesmí končit dřív než začne.</sch:title>
        <sch:rule context="gd:deal">
            <sch:report test="xs:dateTime(gd:starts) &gt;= xs:dateTime(gd:ends)">Deal nesmí končit dřív než začne.</sch:report>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:title>Datum vydani nesmi byt nesmysl.</sch:title>
        <sch:rule context="gd:game">
            <sch:report test="xs:date(gd:releaseDate) &lt;= xs:date('1970-01-01')">Release date nesmi byt nesmysl.</sch:report>
        </sch:rule>
    </sch:pattern>
    
</sch:schema>