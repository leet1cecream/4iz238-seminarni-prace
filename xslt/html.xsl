<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:gd="urn:vse:feja02:gameDeals"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output method="html" version="5"/>
    
    <xsl:template match="/">
        <html lang="en">
            <head>
                <title>GameDeals</title>
                <description></description>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous"/>
                <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
            </head>
            <body class="d-flex flex-column justify-content-start">
                
                <div class="flex-grow-1 w-75 mx-auto mb-5">
                    <div class="header text-center pt-4 pb-2">
                        <a class="text-decoration-none" href="./index.html">
                            <img src="../resources/logo.png" width="300"></img>
                        </a>
                        <hr/>
                    </div>
                    <xsl:apply-templates select="gd:gameDealsDatabase/gd:deals"/>
                    <xsl:apply-templates select="gd:gameDealsDatabase/gd:stores"/>
                    <xsl:apply-templates select="gd:gameDealsDatabase/gd:games"/>
                </div>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="gd:deals">
        <div>
            <div class="text-center mb-3">
                <h3>Deals</h3>
            </div>
            <xsl:choose>
                <xsl:when test="count(/gd:gameDealsDatabase/gd:deals/gd:deal) &lt; 1">
                    <h4>No deals found</h4>
                </xsl:when>
                <xsl:otherwise>
                    <div>
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>Game</th>
                                    <th>DRM</th>
                                    <th>Price</th>
                                    <th>Starts</th>
                                    <th>Ends</th>
                                    <th>Link</th>
                                </tr>
                            </thead>
                            <tbody>
                                <xsl:for-each select="./gd:deal">
                                    <tr>
                                        <td><xsl:value-of select="./gd:game"/></td>
                                        <td><xsl:value-of select="./gd:drm"/></td>
                                        <td><strike><xsl:value-of select="./gd:price/gd:currentPrice"/></strike>&#160;&#160;<xsl:value-of select="./gd:price/gd:fullPrice"/><xsl:value-of select="./gd:price/gd:currency"/></td>
                                        <xsl:variable name="starts" select="./gd:starts" />
                                        <xsl:variable name="year" select="substring($starts, 1, 4)" />
                                        <xsl:variable name="month" select="substring($starts, 6, 2)" />
                                        <xsl:variable name="day" select="substring($starts, 9, 2)" />
                                        <xsl:variable name="time" select="substring($starts, 12, 8)" />
                                        <td><xsl:value-of select="concat($year, '-', $month, '-', $day, ' ', $time)"/></td>
                                        <xsl:variable name="ends" select="./gd:ends" />
                                        <xsl:variable name="year" select="substring($ends, 1, 4)" />
                                        <xsl:variable name="month" select="substring($ends, 6, 2)" />
                                        <xsl:variable name="day" select="substring($ends, 9, 2)" />
                                        <xsl:variable name="time" select="substring($ends, 12, 8)" />
                                        <td><xsl:value-of select="concat($year, '-', $month, '-', $day, ' ', $time)"/></td>
                                        <td><a href="{./gd:url}">Link</a></td>
                                    </tr>
                                </xsl:for-each>
                            </tbody>
                        </table>
                    </div>
                </xsl:otherwise>
            </xsl:choose>
        </div>
    </xsl:template>

    <xsl:template match="gd:stores">
        <div>
            <div class="text-center mb-5">
                <h3 class="mt-5">Stores</h3>
            </div>
            <div class="row">
                <xsl:for-each select="gd:store">
                    <div class="card m-1" style="width: 14rem;">
                        <img src="../{./gd:logo/@src}" class="card-img-top" alt="Ubisoft Store Logo"/>
                            <div class="card-body">
                                <h5 class="card-title"><xsl:value-of select="./gd:name"/></h5>
                                <p class="card-text">Owner: <xsl:value-of select="./gd:owner"/></p>
                                <a href="{./gd:url}" class="btn btn-primary">Visit Store</a>
                                <a href="{./gd:contact}" class="btn btn-secondary">Contact</a>
                            </div>
                    </div>
                </xsl:for-each>
            </div>
        </div>
    </xsl:template>
    
    <xsl:template match="gd:games">
        <div>
            <div class="text-center mb-5">
                <h3 class="mt-5">Games</h3>
            </div>
            <xsl:choose>
                <xsl:when test="count(/gd:gameDealsDatabase/gd:games/gd:game) &lt; 1">
                    <h4>No games found</h4>
                </xsl:when>
                <xsl:otherwise>
                    <div>
                        <ul>
                            <xsl:for-each select="gd:game">
                                <xsl:sort select="current()" order="ascending"/>
                                <li>
                                    <a href="game/{generate-id()}.html"><xsl:value-of select="./gd:name"/></a>
                                </li>
                                
                                <xsl:result-document href="game/{generate-id()}.html">
                                    <html lang="en">
                                        <head>
                                            <title>GameDeals</title>
                                            <description></description>
                                            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous"/>
                                            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous"></script>
                                        </head>
                                        <body class="d-flex flex-column justify-content-start">
                                            
                                            <div class="flex-grow-1 w-75 mx-auto mb-5">
                                                <div class="header text-center pt-4 pb-2">
                                                    <a class="text-decoration-none" href="../index.html">
                                                        <img src="../../resources/logo.png" width="300"></img>
                                                    </a>
                                                    <hr/>
                                                </div>
                                                <div class="card text-center" style="width: 64rem;">
                                                    <div class="card-body">
                                                        <h5 class="card-title"><xsl:value-of select="gd:name"/></h5>
                                                        <p class="card-text">Genres: <xsl:value-of select="gd:genres"/></p>
                                                        <p class="card-text">Release Date: <xsl:value-of select="xs:date(gd:releaseDate)"/></p>
                                                        <p class="card-text">Description: <xsl:value-of select="gd:description"/></p>
                                                        <p class="card-text">Developers: <xsl:value-of select="gd:developers"/></p>
                                                        <p class="card-text">Publishers: <xsl:value-of select="gd:publishers"/></p>
                                                        <h6 class="card-subtitle mb-2 text-muted">System Requirements:</h6>
                                                        <ul class="list-group">
                                                            <li class="list-group-item">Operating System: <xsl:value-of select="gd:requirements/gd:os"/></li>
                                                            <li class="list-group-item">Processor: <xsl:value-of select="gd:requirements/gd:processor"/></li>
                                                            <li class="list-group-item">Memory: <xsl:value-of select="gd:requirements/gd:memory"/> <xsl:value-of select="gd:requirements/gd:memory/@units"/></li>
                                                            <li class="list-group-item">Graphics: <xsl:value-of select="gd:requirements/gd:graphics"/></li>
                                                            <li class="list-group-item">Storage: <xsl:value-of select="gd:requirements/gd:storage"/> <xsl:value-of select="gd:requirements/gd:storage/@units"/></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </body>
                                    </html>
                                </xsl:result-document>
                            </xsl:for-each>
                        </ul>
                    </div>
                </xsl:otherwise>
            </xsl:choose>
        </div>
    </xsl:template>

</xsl:stylesheet>