<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:gd="urn:vse:feja02:gameDeals"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output method="xml"/>
    
    <xsl:template match="/">
        <fo:root>
            <fo:layout-master-set>
                <fo:simple-page-master master-name="obsah-A4" page-height="297mm" page-width="210mm"
                    margin="20mm">
                    <fo:region-body/>
                    <fo:region-before/>
                    <fo:region-after/>
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="obsah-A4">
                <fo:static-content flow-name="xsl-region-before">
                    <fo:block>GameDeals</fo:block>
                </fo:static-content>
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block>Page <fo:page-number/></fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <xsl:call-template name="toc"></xsl:call-template>
                    <xsl:apply-templates select="gd:gameDealsDatabase/gd:stores"></xsl:apply-templates>
                    <xsl:apply-templates select="gd:gameDealsDatabase/gd:games"></xsl:apply-templates>
                    <xsl:apply-templates select="gd:gameDealsDatabase/gd:deals"></xsl:apply-templates>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    
    <xsl:template name="toc">
        <fo:block break-before='page' break-after='page' margin-top="5mm">
            <fo:block font-size="16pt" font-weight="bold">TABLE OF CONTENTS</fo:block>
            <fo:block text-align-last="justify">
                <fo:basic-link internal-destination="Stores" font-size="150%">
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="'Stores'" />
                    <fo:leader leader-pattern="dots" />
                    <fo:page-number-citation ref-id="Stores" />
                </fo:basic-link>
            </fo:block>
            <xsl:for-each select="gd:gameDealsDatabase/gd:stores/gd:store">
                <fo:block text-align-last="justify">
                    <fo:basic-link internal-destination="{generate-id(.)}">
                        <xsl:text></xsl:text>
                        <xsl:value-of select="gd:name" />
                        <fo:leader leader-pattern="dots" />
                        <fo:page-number-citation ref-id="{generate-id(.)}" />
                    </fo:basic-link>
                </fo:block>
            </xsl:for-each>
            <fo:block text-align-last="justify">
                <fo:basic-link internal-destination="Games" font-size="150%">
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="'Games'" />
                    <fo:leader leader-pattern="dots" />
                    <fo:page-number-citation ref-id="Games" />
                </fo:basic-link>
            </fo:block>
            <xsl:for-each select="gd:gameDealsDatabase/gd:games/gd:game">
                <fo:block text-align-last="justify">
                    <fo:basic-link internal-destination="{generate-id(.)}">
                        <xsl:text></xsl:text>
                        <xsl:value-of select="gd:name" />
                        <fo:leader leader-pattern="dots" />
                        <fo:page-number-citation ref-id="{generate-id(.)}" />
                    </fo:basic-link>
                </fo:block>
            </xsl:for-each>
            <fo:block text-align-last="justify">
                <fo:basic-link internal-destination="Deals" font-size="150%">
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="'Deals'" />
                    <fo:leader leader-pattern="dots" />
                    <fo:page-number-citation ref-id="Deals" />
                </fo:basic-link>
            </fo:block>
            <xsl:for-each select="gd:gameDealsDatabase/gd:deals/gd:deal">
                <fo:block text-align-last="justify">
                    <fo:basic-link internal-destination="{generate-id(.)}">
                        <xsl:text></xsl:text>
                        <xsl:value-of select="gd:game"/>, 
                        <xsl:value-of select="gd:store"/>, 
                        <xsl:value-of select="gd:drm"/>
                        <fo:leader leader-pattern="dots" />
                        <fo:page-number-citation ref-id="{generate-id(.)}" />
                    </fo:basic-link>
                </fo:block>
            </xsl:for-each>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="gd:stores">
        <fo:block page-break-after="always" margin-top="5mm">
            <fo:block font-size="200%"  id="Stores">Stores</fo:block>
            
            <fo:list-block>
                <xsl:apply-templates select="./gd:store"/>
            </fo:list-block>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="gd:store">
        <fo:list-item id="{generate-id(.)}">
            <fo:list-item-label>
                <fo:block/>
            </fo:list-item-label>
            <fo:list-item-body>
                <fo:block background-color="#E8E8E8" margin-bottom="10px">
                    <fo:block font-size="175%">
                        <xsl:value-of select="./gd:name"/>
                    </fo:block>
                    <fo:block>
                        <xsl:value-of select="'Owner:'"/>
                        <xsl:value-of select="./gd:owner"/>
                    </fo:block>
                    <fo:block>
                        <xsl:value-of select="'Url:'"/>
                        <xsl:value-of select="./gd:url"/>
                    </fo:block>
                    <fo:block>
                        <xsl:value-of select="'Contact:'"/>
                        <xsl:value-of select="./gd:contact"/>
                    </fo:block>
                </fo:block>
            </fo:list-item-body>
        </fo:list-item>
    </xsl:template>
    
    <xsl:template match="gd:games">
        <fo:block page-break-after="always" margin-top="5mm">
            <fo:block font-size="200%"  id="Games">Games</fo:block>
            
            <fo:list-block>
                <xsl:apply-templates select="./gd:game"/>
            </fo:list-block>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="gd:game">
        <fo:list-item id="{generate-id(.)}">
            <fo:list-item-label>
                <fo:block/>
            </fo:list-item-label>
            <fo:list-item-body>
                <fo:block background-color="#E8E8E8" margin-bottom="10px">
                    <fo:block font-size="175%">
                        <xsl:value-of select="./gd:name"/>
                    </fo:block>
                    <fo:block>
                        <xsl:value-of select="'Short Description:'"/>
                        <xsl:value-of select="./gd:descriptionShort"/>
                    </fo:block>
                    
                    <fo:block>
                        <xsl:value-of select="'Description:'"/>
                        <xsl:value-of select="./gd:description"/>
                    </fo:block>
                    
                    <fo:block>
                        <xsl:value-of select="'Release Date:'"/>
                        <xsl:value-of select="./gd:releaseDate"/>
                    </fo:block>
                    
                    <fo:block>
                        <xsl:value-of select="'Genres:'"/>
                        <xsl:value-of select="./gd:genres"/>
                    </fo:block>
                    
                    <fo:block>
                        <xsl:value-of select="'Developers:'"/>
                        <xsl:value-of select="./gd:developers"/>
                    </fo:block>
                    
                    <fo:block>
                        <xsl:value-of select="'Publishers:'"/>
                        <xsl:value-of select="./gd:publishers"/>
                    </fo:block>
                    
                    <fo:block>
                        <xsl:value-of select="'System Requirements:'"/>
                        <fo:block>
                            <xsl:value-of select="'OS:'"/>
                            <xsl:value-of select="./gd:requirements/os"/>
                        </fo:block>
                        <fo:block>
                            <xsl:value-of select="'Processor:'"/>
                            <xsl:value-of select="./gd:requirements/processor"/>
                        </fo:block>
                        <fo:block>
                            <xsl:value-of select="'Memory:'"/>
                            <xsl:value-of select="./gd:requirements/memory"/>
                            <xsl:text> GB</xsl:text>
                        </fo:block>
                        <fo:block>
                            <xsl:value-of select="'Graphics:'"/>
                            <xsl:value-of select="./gd:requirements/graphics"/>
                        </fo:block>
                        <fo:block>
                            <xsl:value-of select="'Storage:'"/>
                            <xsl:value-of select="./gd:requirements/storage"/>
                            <xsl:text> GB</xsl:text>
                        </fo:block>
                    </fo:block>
                    
                </fo:block>
            </fo:list-item-body>
        </fo:list-item>
    </xsl:template>
    
    <xsl:template match="gd:deals">
        <fo:block page-break-after="always" margin-top="5mm">
            <fo:block font-size="200%"  id="Deals">Deals</fo:block>
            
            <fo:list-block>
                <xsl:apply-templates select="./gd:deal"/>
            </fo:list-block>
        </fo:block>
    </xsl:template>
    
    <xsl:template match="gd:deal">
        <fo:list-item id="{generate-id(.)}">
            <fo:list-item-label>
                <fo:block/>
            </fo:list-item-label>
            <fo:list-item-body>
                <fo:block background-color="#E8E8E8" margin-bottom="10px">
                    <fo:block font-size="175%">
                        <xsl:value-of select="./gd:game"/>
                    </fo:block>
                    
                    <fo:block>
                        <xsl:value-of select="'Store:'"/>
                        <xsl:value-of select="./gd:store"/>
                    </fo:block>
                    
                    <fo:block>
                        <xsl:value-of select="'DRM:'"/>
                        <xsl:value-of select="./gd:drm"/>
                    </fo:block>
                    
                    <fo:block>
                        <xsl:value-of select="'URL:'"/>
                        <xsl:value-of select="./gd:url"/>
                    </fo:block>
                    
                    <fo:block>
                        <xsl:value-of select="'Price:'"/>
                        <xsl:value-of select="./gd:price/currency"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="./gd:price/currentPrice"/>
                        <xsl:text> (Full price: </xsl:text>
                        <xsl:value-of select="./gd:price/fullPrice"/>
                        <xsl:text>)</xsl:text>
                    </fo:block>
                    
                    <fo:block>
                        <xsl:value-of select="'Starts:'"/>
                        <xsl:value-of select="./gd:starts"/>
                    </fo:block>
                    
                    <fo:block>
                        <xsl:value-of select="'Ends:'"/>
                        <xsl:value-of select="./gd:ends"/>
                    </fo:block>
                    
                </fo:block>
            </fo:list-item-body>
        </fo:list-item>
    </xsl:template>
    
</xsl:stylesheet>